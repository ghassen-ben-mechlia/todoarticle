export interface Article {
    _id?: string;
    titre: string;
    contenu: string;
  }
  