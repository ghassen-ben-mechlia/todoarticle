import { App } from '@/app';
import { AuthRoute } from '@routes/auth.route';
import { UserRoute } from '@routes/users.route';
import { ValidateEnv } from '@utils/validateEnv';
import { ArticleRoute } from './routes/article.route';

ValidateEnv();

const app = new App([new UserRoute(), new AuthRoute(),new ArticleRoute()]);

app.listen();
