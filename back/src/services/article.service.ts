import { hash } from 'bcrypt';
import { Service } from 'typedi';
import { HttpException } from '@exceptions/httpException';
import { Article } from '@interfaces/article.interface';
import { ArticleModel } from '@models/article.model';

@Service()
export class ArticleService {
  public async findAllArticle(): Promise<Article[]> {
    const articles: Article[] = await ArticleModel.find();
    return articles;
  }

  public async findArticleById(articleId: string): Promise<Article> {
    const findArticle: Article = await ArticleModel.findOne({ _id: articleId });
    if (!findArticle) throw new HttpException(409, "Article doesn't exist");

    return findArticle;
  }

  public async createArticle(ArticleData: Article): Promise<Article> {
   
    const createArticleData: Article = await ArticleModel.create({ ...ArticleData });

    return createArticleData;
  }

  public async updateArticle(ArticleId: string, ArticleData: Article): Promise<Article> {  
    
    
    const updateArticleById: Article = await ArticleModel.findByIdAndUpdate(ArticleId, ArticleData, { new: true });
    if (!updateArticleById) throw new HttpException(409, "Article doesn't exist");

    return updateArticleById;
  }

  public async deleteArticle(ArticleId: string): Promise<Article> {
    const deleteArticleById: Article = await ArticleModel.findByIdAndDelete(ArticleId);
    if (!deleteArticleById) throw new HttpException(409, "Article doesn't exist");

    return deleteArticleById;
  }
}
