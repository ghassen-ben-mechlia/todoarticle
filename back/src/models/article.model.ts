import { model, Schema, Document } from 'mongoose';
import { Article } from '@/interfaces/article.interface';

const ArticleSchema: Schema = new Schema({
    titre: {
    type: String,
    required: true,
  },
  contenu: {
    type: String,
    required: true,
  },
});

export const ArticleModel = model<Article & Document>('Article', ArticleSchema);
