import { IsEmail, IsString, IsNotEmpty, MinLength, MaxLength } from 'class-validator';

export class CreateArticleDto {
 
    @IsString()
    @IsNotEmpty()

    public contenu: string;

  @IsString()
  @IsNotEmpty()
 
  public titre: string;
}

export class UpdateArticleDto {
    @IsString()
    @IsNotEmpty()
    
    public contenu: string;
  @IsString()
  @IsNotEmpty()
  
  public titre: string;
}