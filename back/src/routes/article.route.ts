import { Router } from 'express';
import { ArticleController } from '@controllers/articles.controller';
import { CreateArticleDto } from '@dtos/article.dto';
import { Routes } from '@interfaces/routes.interface';
import { ValidationMiddleware } from '@middlewares/validation.middleware';

export class ArticleRoute implements Routes {
  public path = '/articles';
  public router = Router();
  public Article = new ArticleController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.Article.getArticles);
    this.router.get(`${this.path}/:id`, this.Article.getArticleById);
    this.router.post(`${this.path}`,  this.Article.createArticle);
    this.router.put(`${this.path}/:id`, this.Article.updateArticle);
    this.router.delete(`${this.path}/:id`, this.Article.deleteArticle);
  }
}
