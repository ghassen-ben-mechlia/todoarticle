import { NextFunction, Request, Response } from 'express';
import { Container } from 'typedi';
import { Article } from '@interfaces/article.interface';
import { ArticleService } from '@services/article.service'

export class ArticleController {
  public Article = Container.get(ArticleService);

  public getArticles = async (req: Request, res: Response, next: NextFunction) => {
    try {

      const findAllArticlesData: Article[] = await this.Article.findAllArticle();

      res.status(200).json({ data: findAllArticlesData, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getArticleById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const ArticleId: string = req.params.id;
      const findOneArticleData: Article = await this.Article.findArticleById(ArticleId);

      res.status(200).json({ data: findOneArticleData, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createArticle = async (req: Request, res: Response, next: NextFunction) => {
    try {
      
      
      const ArticleData: Article = req.body;
      console.log(ArticleData);
      const createArticleData: Article = await this.Article.createArticle(ArticleData);

      res.status(201).json({ data: createArticleData, message: 'created' });
    } catch (error) {
      console.log("error",error);
      
      next(error);
    }
  };

  public updateArticle = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const ArticleId: string = req.params.id;
      const ArticleData: Article = req.body;
      const updateArticleData: Article = await this.Article.updateArticle(ArticleId, ArticleData);

      res.status(200).json({ data: updateArticleData, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteArticle = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const ArticleId: string = req.params.id;
      const deleteArticleData: Article = await this.Article.deleteArticle(ArticleId);

      res.status(200).json({ data: deleteArticleData, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}
