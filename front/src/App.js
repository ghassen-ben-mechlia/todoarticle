import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import RequireAuth from "./components/RequireAuth";
import DashboardLayout from "./components/DashboardLayout";

import Home from "./pages/Home";
import UpdateProfile from "./pages/UpdateProfile";



const App = () => {
  return (
    <Router>
      <div className="app">
        <Routes>
          <Route
            path="/"
            element={<LoginPage />}
          />
                  <Route element={<RequireAuth  />}>
                  <Route element={<DashboardLayout  />}>

                  <Route
            path="/home"
            element={<Home />}
          />
                <Route
            path="/update-profile"
            element={<UpdateProfile />}
          />
</Route>
</Route>

        </Routes>
      </div>
    </Router>
  );
};

export default App;
