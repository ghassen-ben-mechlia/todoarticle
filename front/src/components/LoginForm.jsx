import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Formik, Form, ErrorMessage } from "formik";
import { ToastContainer } from "react-toastify";
import { useNavigate } from "react-router-dom";

import * as Yup from "yup";
import {
  Button,
  Row,
  Input,
  InputGroup,
  InputGroupText,
} from "reactstrap";

import { FiEye, FiEyeOff } from "react-icons/fi";
import { loginAsync } from "../apis/auth/authSlice";
const LoginForm = () => {
    const [showPassword, setShowPassword] = useState(false); // State to toggle password visibility
    const dispatch=useDispatch()
    const navigate=useNavigate()
    const validationSchema = Yup.object().shape({
        email: Yup.string()
          .email("Invalid email address")
          .required("Email is required")
          .test(
            "is-societe-email",
            "Email must end with @gmail.com",
            (value) => {
              if (value) {
                return value.endsWith("@gmail.com");
              }
              return true; // Allow empty value
            }
          ),
        password: Yup.string().min(9).required("Password is required"),
      });
    const handleSubmit = (values) => {
dispatch(loginAsync(values,navigate)) 
  // Perform login logic here
      };
    
      return (
        <>
        <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ handleChange, values }) => (
          <Form className="hunter-form">
            <Input
              size={"lg"}
              type="email"
              id="email"
              placeholder="Email"
              className={"email"}
              name="email"
              value={values.email}
              onChange={handleChange}
            />
            <ErrorMessage
              name="email"
              component="div"
              className="error-message"
            />
            <InputGroup>
              <Input
                size={"lg"}
                type={showPassword ? "text" : "password"}
                name="password"
                value={values.password}
                onChange={handleChange}
                placeholder="Password"
              />
              <InputGroupText
                onClick={() => setShowPassword(!showPassword)}
                className={`eye-icon`}
              >
                {showPassword ? <FiEye /> : <FiEyeOff />}
              </InputGroupText>
            </InputGroup>
            <ErrorMessage
              name="confirmPassword"
              component="div"
              className="error-message-update"
            />
            <ErrorMessage
              name="password"
              component="div"
              className="error-message"
            />
            <Row>
              <Button
                className="btn-forget-password"
                // onClick={() => navigate("/forget-password")}
              >
                Forgot Password ?
              </Button>
            </Row>
            <Row>
              <Button type="submit" className="submit-form">
                Sign in
              </Button>
            </Row>
          </Form>
        )}
      </Formik>
              <ToastContainer />
</>
      );
}

export default LoginForm