import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer } from "react-toastify";
import { ErrorMessage, Formik, Form } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import { Col, Row, Input, Label, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";



import avatar from "../assets/images/avatar.png";
import { getOneUserAsync, updateUsersAsync } from "../apis/users/usersSlice";
const validationSchema = Yup.object().shape({
  email: Yup.string().required("email Required"),
  password: Yup.string().required("password Required"),

});

export default function ProfileForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [countrychoice, setcountrychoice] = useState(null);
  const [profilePicture, setProfile_Photo] = useState(null);
  const [rolechoice, setRolechoice] = useState(null);

  useEffect(() => {
    dispatch(getOneUserAsync(JSON.parse(localStorage.getItem("data"))._id));
  }, [dispatch]);

  const handleAddOrUpdateUser = (values, setSubmitting) => {
    dispatch(updateUsersAsync(values,JSON.parse(localStorage.getItem('data'))?._id))
    // dispatch(
    //   updateProfilAsync(
    //     {
    //       id: user._id,
    //       country: countrychoice?.value,
    //       profilePicture:
    //         profilePicture === null ? user.profilePicture : profilePicture,
    //       ...values,
    //     },
    //     navigate
    //   )
    // );
  };

  return (
    <div>
      <Formik
        initialValues={{
          email: JSON.parse(localStorage.getItem('data')).email,
          password: "",
          
        }}
        enableReinitialize
        validationSchema={validationSchema}
        onSubmit={handleAddOrUpdateUser}
      >
        {({ isSubmitting, handleChange, values }) => (
          <Form className="hunter-form" id="update-profile">
            <Row>
              <Col className="d-none d-xl-block"></Col>
              <Col md={5}>
                <div className="profile-image-uploader">
                  <div className="image-preview">
                    <div
                      style={{
                        border: "1px solid #40bd3921",
                        borderRadius: "15px",
                        width: "initial",
                        height: "100%",
                        backgroundImage: `url("${
                             avatar
                        }")`,
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center",
                        backgroundColor: "transparent",
                      }}
                    ></div>

                    <label
                      htmlFor="image-change"
                      className="custom-file-upload"
                    >
                      <FontAwesomeIcon icon={faPen} size="1x" className="" />
                    </label>
                    {/* <input
                      id="image-change"
                      type="file"
                      onChange={(e) => {
                        setProfile_Photo(e.target.files[0]);
                        console.log(e.target.files[0]);
                      }}
                      accept="image/*"
                    /> */}
                  </div>
                </div>
              </Col>
              <Col md={1}></Col>
              <Col md={5}>
              <Row>
                  <Label>Email Address</Label>
                  <Input
                    size={"md"}
                    type="text"
                    name="email"
                    value={values.email}
                    onChange={handleChange}

                  />
                </Row>
                <Row>
                  <Label>Password</Label>
                  <Input
                    size={"md"}
                    type="password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}

                  />
                </Row>
              </Col>
              <Col className="d-none d-xl-block"></Col>
            </Row>
          
            <Row>
              <Button
                disabled={isSubmitting}
                className="btn-update"
                type="submit"
              >
                Update
              </Button>
            </Row>
          </Form>
        )}
      </Formik>
      <ToastContainer />
    </div>
  );
}
