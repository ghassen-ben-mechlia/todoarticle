import React, { useEffect, useState } from "react";

import {
  Button,
  Modal,
  ModalBody,
  Input,
  Label,
  Form,
  FormGroup,
} from "reactstrap";
import { FiX } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { addarticlesAsync, updatearticlesAsync } from "../apis/articles/articlesSlice";
// import { getJobopeningAsync, updateobopeninghirigAsync } from "../../../features/openings/openingsSlice";

function ModalAddTask({
  modal,
  toggle,
  className,
  onClose,
  edit
}) {
  const [taskTitle, setTaskTitle] = useState("");
  const [taskSummary, setTaskSummary] = useState("");
  const [candidatetosend, setCandidatetosend] = useState(null);
  const article = useSelector((state) => state.articles.article);
  const dispatch = useDispatch();

  useEffect(() => {
    // If 'edit' is true, set the initial state using article values
    if (edit) {
     
      setTaskTitle(article.titre);
      setTaskSummary(article.contenu);
    }
  }, [edit, article]);

  return (
    <Modal isOpen={modal} toggle={toggle} className={className}>
      {/* ... (other parts of your component) */}
      <ModalBody>
        <h3>{edit ? "Edit Task" : "Add Task"}</h3>
        <Form>
          <FormGroup>
            <Label>Summary:</Label>
            <Input
              name="summary"
              value={taskSummary}
              defaultValue={taskSummary}
              onChange={(e) => setTaskSummary(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Title:</Label>
            <Input
              name="title"
              value={taskTitle}
              defaultValue={taskTitle}
              onChange={(e) => setTaskTitle(e.target.value)}
            />
          </FormGroup>
          <div className="mail-div">
            <Button
              className="send-mail"
              onClick={() => {
                if (edit) {
                  dispatch(updatearticlesAsync({ titre: taskTitle, contenu: taskSummary },article._id))
                 
                } else {
                  dispatch(addarticlesAsync({ titre: taskTitle, contenu: taskSummary }));
                }
                toggle()
                setTaskSummary('')
                setTaskTitle('')
              }}
            >
              {edit ? "Edit Task" : "Add to Job"}
            </Button>
          </div>
        </Form>
      </ModalBody>
    </Modal>
  );
}


export default ModalAddTask;
