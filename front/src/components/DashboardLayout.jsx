import React from 'react'
import { Container } from "reactstrap";
import MainComponent from './MainComponent'
const DashboardLayout = () => {
  return (
<Container fluid id="dashboard-layout">
    
      <MainComponent  />
    </Container>  )
}

export default DashboardLayout