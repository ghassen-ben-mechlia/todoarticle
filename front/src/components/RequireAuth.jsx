
import { useLocation, Navigate, Outlet } from "react-router-dom";
import {  useSelector } from "react-redux";
import { useEffect } from "react";


const RequireAuth = ({ allowedRoles }) => {

    const location = useLocation();
   
    return (
        JSON.parse(localStorage.getItem('data'))
            ? <Outlet /> : <Navigate to="/" state={{ from: location }} replace />
    );
}

export default RequireAuth;