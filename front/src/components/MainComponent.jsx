import React from "react";
import { Container } from "reactstrap";
import { Outlet } from "react-router-dom";
import Header from "./Header";

function MainComponent({ isOpen }) {
  return (
    <Container
      fluid
      id="main-component"
    >
      <Header />
      <div id="outlet">
        <Outlet />
      </div>
    </Container>
  );
}

export default MainComponent;
