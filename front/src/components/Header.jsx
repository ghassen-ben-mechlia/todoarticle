import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Button } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import Notification from "../assets/images/icons/notification.png";
import avatar from "../assets/images/Kayla-Person.jpg";

import { getOneUserAsync } from "../apis/users/usersSlice";
function Header() {
  const user = useSelector((state) => state.users.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleUpdateProfile = () => {
    navigate("/update-profile");
  };

  useEffect(() => {
    dispatch(
      getOneUserAsync(
        localStorage.getItem("data") &&
          JSON.parse(localStorage.getItem("data"))._id
      )
    );
  }, []);
  return (
    <Container fluid id="header">
      <div className="section-notif">
        <img src={Notification} alt="notification" />
      </div>
      <Button className="section-username" onClick={handleUpdateProfile}>
            <img
              src={avatar}
            className="image-profile"
              alt="profilpicture"
            />
          
        <p className="name">{user && user.email}</p>
      </Button>
    </Container>
  );
}

export default Header;
