import React from "react";
import { Container, Row } from "reactstrap";
import LoginForm from "../components/LoginForm";

function LoginPage({ formComponent }) {
  return (
    <Container id="login-page" fluid>
      <Container id="login-form">
        <Row className="top-section">
          <h2 className="title">Welcome To HUNTER</h2>
          <h3 className="sub-title">“The Talent Intelligence Platform”</h3>
        </Row>
        <Row>
          <LoginForm />
        </Row>
      </Container>
    </Container>
  );
}

export default LoginPage;
