import React, { useState } from "react";
import {
  Container,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import ProfileForm from "../components/ProfilForm";

function UpdateProfile() {
  const [currentActiveTab, setCurrentActiveTab] = useState("1");
  const toggle = (tab) => {
    if (currentActiveTab !== tab) setCurrentActiveTab(tab);
  };
  return (
    <Container fluid id="update-profile">
      <Nav tabs>
        <NavItem>
          <NavLink
            className={currentActiveTab == "1" ? "active" : ""}
            onClick={() => {
              toggle("1");
            }}
          >
            Personnal Information
          </NavLink>
        </NavItem>
      
      </Nav>
      <TabContent activeTab={currentActiveTab}>
        <TabPane tabId="1">
          <ProfileForm />
        </TabPane>
      
      </TabContent>
    </Container>
  );
}

export default UpdateProfile;
