import React from 'react'
import {
	Container,
	Text,
	Title,
	Group,
	Card,
	ActionIcon,
} from '@mantine/core';
import {
    Button,
  
  } from "reactstrap";
import { useState, useEffect } from 'react';
import {useDispatch,useSelector} from 'react-redux'
import {  Edit, Trash } from 'tabler-icons-react';

import {
	MantineProvider,

} from '@mantine/core';


import { useHotkeys, useLocalStorage } from '@mantine/hooks';
import ModalAddTask from '../components/ModalAddTask';
import { deletearticlesAsync, getOnearticleAsync, getarticlesAsync } from '../apis/articles/articlesSlice';
const Home = () => {
	const [opened, setOpened] = useState(false);
    const [edit, setEdit] = useState(false);


	const [colorScheme, setColorScheme] = useLocalStorage({
		key: 'mantine-color-scheme',
		defaultValue: 'light',
		getInitialValueInEffect: true,
	});
    const data=useSelector((state)=>state.articles.data)
    const dispatch=useDispatch()
	const toggleColorScheme = value =>
		setColorScheme(value || (colorScheme === 'dark' ? 'light' : 'dark'));

	useHotkeys([['mod+J', () => toggleColorScheme()]]);

	
    const toggle = () => {
        setOpened(!opened)
      };
	

	

	
	

	useEffect(() => {
		dispatch(getarticlesAsync())
	}, []);
    useEffect(() => {
		console.log(data);
	}, [data]);
  return (
   
    <MantineProvider
        theme={{ colorScheme, defaultRadius: 'md' }}
        withGlobalStyles
        withNormalizeCSS>
        <div className='App'>
           <ModalAddTask modal={opened}   edit={edit}    toggle={toggle}  onClose={toggle}     className="email-popup"
/>
            <Container size={550} my={40}>
                <Group position={'apart'}>
                    <Title
                        sx={theme => ({
                            fontFamily: `Greycliff CF, ${theme.fontFamily}`,
                            fontWeight: 900,
                        })}>
                        My Tasks
                    </Title>
                
                </Group>
                {data?.length > 0 ? (
                    data?.map((task, index) => {
                        if (task.titre) {
                            return (
                                <Card withBorder key={index} mt={'sm'} style={{border:"1px solid red",boxShadow:'rgba(0, 0, 0, 0.1) 0px 1px 3px 0px',margin:"5px",padding:"2%"}}>
                                    <Group position={'apart'} style={{display:"flex",width:"100%",justifyContent:"space-between"}}>
                                        <Text weight={'bold'}>{task.titre}</Text>
                                        <div>
                                        <ActionIcon
                                            onClick={() => {
                                                dispatch(deletearticlesAsync(task._id))
                                            }}
                                            color={'red'}
                                            variant={'transparent'}>
                                            <Trash />
                                        </ActionIcon>
                                        <ActionIcon
                                            onClick={() => {
                                                setOpened(true)
                                                dispatch(getOnearticleAsync(task._id))
                                                setEdit(true)
                                            }}
                                            color={'black'}
                                            variant={'transparent'}>
                                            <Edit />
                                        </ActionIcon>
                                        </div>
                                      
                                    </Group>
                                    <Text color={'dimmed'} size={'md'} mt={'sm'}>
                                        {task.contenu
                                            ? task.contenu
                                            : 'No summary was provided for this task'}
                                    </Text>
                                </Card>
                            );
                        }
                    })
                ) : (
                    <Text size={'lg'} mt={'md'} color={'dimmed'}>
                        You have no tasks
                    </Text>
                )}
                <Button
                    onClick={() => {
                        console.log("dd");
                        setOpened(true);
                    }}
                    fullWidth
                    mt={'md'}>
                    New Task
                </Button>
            </Container>
        </div>
    </MantineProvider>
  )
}

export default Home