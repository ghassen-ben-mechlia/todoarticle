import axios from "axios";

//const  Base_URL="http://192.168.10.104:4000/api/v1"
const  baseURL="http://localhost:4000"
const storgeURL="http://localhost:5000"
const storgeURLGetFile="http://localhost:5000/files/users"

const api = axios.create({
    baseURL: baseURL,
    withCredentials: true,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const apiFormData = axios.create({
    baseURL: baseURL,
    withCredentials: true,
    headers: {
      Accept: "multipart/form-data",
  
      "Content-Type": "multipart/form-data",
    },
  });
  const apiStorageImage = axios.create({
    baseURL: storgeURL,
    withCredentials: true,
    headers: {
      Accept: "*/*",
  
      "Content-Type": "multipart/form-data",
    },
  });
export default  {baseURL,api,apiFormData,apiStorageImage,storgeURL,storgeURLGetFile}