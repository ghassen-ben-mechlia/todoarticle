import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import apiSlice from "../apiSlice";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const articleSlide = createSlice({
  name: "articles",
  initialState: {
    data: [],
    article: {},
    datatosend: null,
    totalPages: 0,
    pageSize: 0,
    pageNumber: 0,
  },
  reducers: {
    getarticles: (state, action) => {
      // const { article, accessToken } = action.payload
      state.data = action.payload;
      // state.token = accessToken
    },
 
    getarticle: (state, action) => {
      state.article = action.payload;
    },
    getTotalPages: (state, action) => {
      state.totalPages = action.payload;
    },
    getPageNumber: (state, action) => {
      state.pageNumber = action.payload;
    },
    getPageSize: (state, action) => {
      state.pageSize = action.payload;
    },
  },
});

export const getarticlesAsync = (data) => async (dispatch) => {
 
    apiSlice.api
      .get(`${apiSlice.baseURL}/articles`, data)
      .then((res) => {
       dispatch(getarticles(res.data.data));
      })
      .catch((err) => {
        console.log(err);
      });

};
export const getOnearticleAsync = (data) => async (dispatch) => {
  apiSlice.api
    .get(`${apiSlice.baseURL}/articles/${data}`)
    .then((res) => {
      dispatch(getarticle(res.data.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
export const addarticlesAsync = (data,resetForm,state) => async (dispatch) => {
 

  
  console.log(data);

  // article clicked "Yes," send the request with the form data
  apiSlice.api
    .post(`${apiSlice.baseURL}/articles`, data)
    .then((result) => {
      // Show a success alert
     dispatch(getarticlesAsync())
      toast.success("The article has been added successfully.", {
        position: toast.POSITION.TOP_RIGHT,
      });

    
    })
    .catch((err) => {
      // Show an error alert
console.log(err);
      toast.error(err.response.data.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    });
};
export const updatearticlesAsync = (data, id) => async (dispatch) => {
  
 
  apiSlice.api
    .put(`${apiSlice.baseURL}/articles/${id}`, data)
    .then((res) => {
      // Show a success alert
     
      dispatch(getarticlesAsync())
      toast.success("article Updated Successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
    })
    .catch((err) => {
      // Show an error alert

      toast.error("There was an error updating the article data.", {
        position: toast.POSITION.TOP_RIGHT,
      });
    });
};

export const deletearticlesAsync = (id) => async (dispatch) => {
  
 
   apiSlice.api
     .delete(`${apiSlice.baseURL}/articles/${id}`)
     .then((res) => {
       // Show a success alert
      
 dispatch(getarticlesAsync())
       toast.success("article Deleted Successfully!", {
         position: toast.POSITION.TOP_RIGHT,
       });
     })
     .catch((err) => {
       // Show an error alert
 
       toast.error("There was an error Deleted the article data.", {
         position: toast.POSITION.TOP_RIGHT,
       });
     });
 };
 


export const {
  getarticles,
  getarticle,
  handleChange,
  handleChangearticleId,
  getTotalPages,
  getPageSize,
  getPageNumber,
} = articleSlide.actions;
export default articleSlide.reducer;
