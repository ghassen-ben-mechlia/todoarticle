import { createSlice } from "@reduxjs/toolkit";
import apiSlice from "../apiSlice";
import { toast } from "react-toastify";




const API_URL = "auth";

export const authSlide = createSlice({
  name: "auth",
  initialState: {
    user: null,
    token: null,
  
    error:""
  },
  reducers: {
    setCredentials: (state, action) => {
      // const { user, accessToken } = action.payload
      state.user = action.payload;
      // state.token = accessToken
    },

  },
});


export const loginAsync = (data,navigate)=> async (dispatch) => {
  apiSlice.api
    .post(`${apiSlice.baseURL}/${API_URL}/signin`, data)
    .then((res) => {
      dispatch(setCredentials(res.data.data ));

      localStorage.setItem("data", JSON.stringify(res.data.data));
      // Access the cookies from the response headers
      toast.success("login with success", {
        position: toast.POSITION.TOP_RIGHT
      });
      navigate('/home')
      // You can store or manipulate the cookies as needed
      // For example, you can extract specific cookie values using regex or a cookie parsing library
    })
    .catch((err) => {
      console.log(err);
    
        toast.error(err.message, {
          position: toast.POSITION.TOP_RIGHT
        });
      
    });
};

export const logoutAsync = () => async (dispatch) => {
  apiSlice.api
    .post(`${apiSlice.baseURL}/${API_URL}/logout`, { withCredentials: true })
    .then(() => {
      localStorage.clear();
      window.location.href = "/";
    })
    .catch((err) => {
      console.log(err);
    });
};

export const { setCredentials,handleChangeError } = authSlide.actions;
export default authSlide.reducer;
