import { configureStore } from "@reduxjs/toolkit";
import  authSlide from "./auth/authSlice";
import  userSlide  from "./users/usersSlice";
import  articleSlide  from "./articles/articlesSlice";





export const store = configureStore({
    reducer: {
        
    
        auth:authSlide,
      users:userSlide, 
      articles:articleSlide     

    }
})