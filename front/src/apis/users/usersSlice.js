import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import apiSlice from "../apiSlice";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const userSlide = createSlice({
  name: "users",
  initialState: {
    data: [],
    user: {},
    datatosend: null,
    totalPages: 0,
    pageSize: 0,
    pageNumber: 0,
  },
  reducers: {
    getUsers: (state, action) => {
      // const { user, accessToken } = action.payload
      state.data = action.payload;
      // state.token = accessToken
    },
 
    getUser: (state, action) => {
      state.user = action.payload;
    },
    getTotalPages: (state, action) => {
      state.totalPages = action.payload;
    },
    getPageNumber: (state, action) => {
      state.pageNumber = action.payload;
    },
    getPageSize: (state, action) => {
      state.pageSize = action.payload;
    },
  },
});

export const getUsersAsync = (data) => async (dispatch) => {
 
    apiSlice.api
      .get(`${apiSlice.baseURL}/users/`, data)
      .then((res) => {
        dispatch(getUsers(res.data.users));
      })
      .catch((err) => {
        console.log(err);
      });

};
export const getOneUserAsync = (data) => async (dispatch) => {
  apiSlice.api
    .get(`${apiSlice.baseURL}/users/${data}`)
    .then((res) => {
      dispatch(getUser(res.data));
    })
    .catch((err) => {
      console.log(err);
    });
};
export const addUsersAsync = (data,resetForm,state) => async (dispatch) => {
 

  

  // User clicked "Yes," send the request with the form data
  apiSlice.apiFormData
    .post(`${apiSlice.baseURL}/users`, data)
    .then((result) => {
      // Show a success alert
      resetForm({
        fullName: state && state.edit ? state.detail.fullName : "",
        jobTitle: state && state.edit ? state.detail.jobTitle : "",
        email: state && state.edit ? state.detail.email : "",
        phone: state && state.edit ? state.detail.phone : "",
        country: state && state.edit ? state.detail.country : "",
      });
      toast.success("The user has been added successfully.", {
        position: toast.POSITION.TOP_RIGHT,
      });

    
    })
    .catch((err) => {
      // Show an error alert
console.log(err);
      toast.error(err.response.data.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
    });
};
export const updateUsersAsync = (data, id) => async (dispatch) => {
  
 
  apiSlice.api
    .put(`${apiSlice.baseURL}/users/${id}`, data)
    .then((res) => {
      // Show a success alert
     

      toast.success("User Updated Successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
    })
    .catch((err) => {
      // Show an error alert

      toast.error("There was an error updating the user data.", {
        position: toast.POSITION.TOP_RIGHT,
      });
    });
};




export const {
  getUsers,
  getUser,
  handleChange,
  handleChangeUserId,
  getTotalPages,
  getPageSize,
  getPageNumber,
} = userSlide.actions;
export default userSlide.reducer;
